<?php

/**
 * @file
 * Documentation for variation API.
 */

/**
 * Register your style.
 
 form_alter callback is function with execute in hook_form_alter().
 dengan fungsi ini, anda bisa menaruh fungsi anda sendiri di dalam property #process
 sehinngga dapat mengoverride hasil yang ada sebelum singgah di drupal_render.
 
 pre_render callback is function with add in #pre_render
 dengan fungsi ini anda bisa menimpa nilai property pada child.
 tapi tidak akan berpengaruh jika element dalam form ini
 berinteraksi dengan ajax.
 
 */
function hook_variation_info() {
  return array(
    array(
      'name' => 'bootstrap_basic_default',
      'title' => 'Basic Default',
    ),
    array(
      'name' => 'bootstrap_table_default',
      'title' => 'Table Default',
    ),
  );
}
