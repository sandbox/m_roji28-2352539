<?php

function variation_admin_page($form, &$form_state) {
  // Nantinya, semua form yang pernah dipanggil dengan hook form alter akan terlihat.
  // $form = array();
  // $form['']
  $form['general'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<h2><small>' . t('Variation of Form') . '</small></h2>',
    '#weight' => -9,
  );  
  $form['node_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Form'),
    '#group' => 'general',
    '#description' => t('variation for base form: node_form.'),
  );
  $form['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other Form'),
    '#group' => 'general',
    '#description' => t('variation for other form_id.'),
  );
  $form['other']['cinta'] = array(
    '#type' => 'markup',
    '#markup' => 'aku',
    // '#group' => 'general',
    // '#description' => t('variation for other form_id.'),
  );
  // $header = array(
		// t('Base Form'),
		// t('Variation'),
		// t('Operations'),
	// );
  // $rows = array(
    // array(
      // 'a',
      // 'a',
      // 'a',
    // ),
  // );
  
  
  $node_form = node_type_get_names();
  foreach ($node_form as $machine => $label) {
    $node_form[$machine . '_node_form'] = $label;
    unset($node_form[$machine]);    
  }
  $node_form = array_merge(array('node_form' => 'Node Form'), $node_form);
  
  $header = array(
		t('Form'),
		t('Variation'),
		t('Operations'),
	);  
  $rows = array();
  $x = 0;
  foreach ($node_form as $key => $type) {
    $columns = array();
    $indentation = '';
    if ($x !== 0) {
      $indentation = theme('indentation', array('size' => 1));
    }
    
		$columns[] = $indentation . $key . ' (<strong>' . $type . '</strong>)';
    $_variable = 'variation_form_' . $key;
    $variable = variable_get($_variable, array());
    $total = count(array_filter($variable));
    // dvm($total,'$total');
    unset($label);    
    if ($x === 0) {
      $label = '-';
    }
    if ($total) {
      $label = format_plural($total, '1 variation', "@count variations");
    }
    isset($label) or $label = '<em>inherit</em>';
    // dpm($label,'$label');
		$columns[] = $label;
    $html_id = 'cell-' . drupal_html_id($key);    
		$columns[] = l(t('edit'), 'admin/config/user-interface/variation/form/edit/' . $key);		
		$rows[] = $columns;
    $x++;
  }
  $form['node_form']['form'] = array(  
		'#theme'  => 'table',
		'#header' => $header,
		'#rows'   => $rows,
		'#empty'  => t('No content type available. <a href="@link">Add content type group</a>.', array('@link' => url('admin/structure/types/add'))),
	);  
  // dpm($form,'$form');
  // return render($form);
  return $form;
}

/**
 * Return array of options that need to selected by user.
 */
function _variation_options() {
  $info = variation_get_variation_info();
  $options = array('_none' => '-');
  foreach ($info as $values) {
    $name = $values['name'];
    $title = $values['title'];
    $options[$name] = $title;    
  }
  return $options;
}

function variation_edit_form_page($form, &$form_state, $form_id) {
  // dpm($id);
  // dpm($form_id,'$form_id');
  // $node_form = node_type_get_names();
  // dpm($node_form,'$node_form');
  drupal_set_title('Edit variation for form id: ' . $form_id);
  // if (!isset($node_form[$form_id])) {
    // drupal_set_message('Form ID not found', 'error');
    // drupal_goto('admin/config/user-interface/variation/form');
  // }
  
  
  $variable = 'variation_form_' . $form_id;
  $variable = variable_get($variable);
  // dpm($variations,'$variations');
  $info = variation_get_variation_info();
  // dpm($info,'$info');
  $list_themes = list_themes();
  // dpm($list_themes,'$list_themes');
  foreach ($info as $theme => $variations) {
    $theme_info = $list_themes[$theme]->info;
    $theme_info_name = $theme_info['name'];
    $form[$theme] = array(
      '#type' => 'fieldset',
      '#title' => t($theme_info_name),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      $form_id => array(
        '#tree' => TRUE,
      ),
    );
    $options = array('' => '- Select -');
    foreach ($variations as $variation) {
      $options[$variation['name']] = $variation['title'];
    }
    $form[$theme][$form_id][$theme] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => isset($variable[$theme]) ? $variable[$theme] : '',
    );
  }
  // dpm($form,'$form');
  
	// $form['test'] = array(
		// '#markup' => 'Pilih style:',
	// );
  // $variable = 'variation_form_' . $form_id;
  // $default_value = variable_get($variable, '_none');
	// $form['style'] = array(
		// '#type' => 'select',
		// '#default_value' => $default_value,
		// '#options' => _variation_options(),
	// );
  // dpm($variable,'$variable');
	// $form['target_form_id'] = array(
		// '#type' => 'hidden',
		// '#value' => $form_id,
		
	// );
	// Action buttons
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
    '#submit' => array('variation_edit_form_page_submit_save'),
  );
  $form['actions']['delete'] = array(
    '#type'  => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('variation_edit_form_page_submit_delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/user-interface/variation/form'),
  );
  $form_state['variation']['form_id'] = $form_id;
	return $form;
}

function variation_edit_form_page_submit_save($form, &$form_state) {
  $form_id = $form_state['variation']['form_id'];  
  $variations = $form_state['values'][$form_id];
  $variable = 'variation_form_' . $form_id;
  variable_set($variable, $variations);
  drupal_set_message(t('Saved.'));
	$form_state['redirect'] = 'admin/config/user-interface/variation/form';
}

function variation_edit_form_page_submit_delete($form, &$form_state) {  
  $form_id = $form_state['variation']['form_id'];
  $variable = 'variation_form_' . $form_id;
  if (!is_null(variable_get($variable))) {
    variable_del($variable);	
    drupal_set_message(t('Deleted.'));
  }
  $form_state['redirect'] = 'admin/config/user-interface/variation/form';  
}
