<?php


/*
Example:
$conditions = array(
  // Optional. Default to 'or'. Available value: 'or', 'and'.
  'operator' => 'or',

  // If you want match element name, use this.
  'name of element' => array(
    // Optional. Default to 'equals'. Available value: 'equals', 'negate'.
    'operator' => 'equals',
    // Required.
    'value' => 'title',
  ),
  // This is alias from above.
  'name of element' => 'title',
  // Deep, kedalaman array.
  // First element means level deep is 0.
  // Child element of first element means level deep is 1.
  'level of deep' => array(
    // Optional. Default to '='. Available value: '=', '>', '<', '>=', '<='.
    'operator' => '=',
    // Required.
    'value' => '2',
  ),
  // This is alias from above.
  'level of deep' => '2',

);
 */
function _variation_modify_element_conditions($element, $conditions) {
  // Empty conditions is true.
  if (!isset($conditions) || !is_array($conditions) || empty($conditions)) {
    return true;
  }

  // Validate operator.
  $operator = isset($conditions['operator']) ? $conditions['operator'] : 'or';
  if (!in_array($operator, array('or', 'and'))) {
    return false;
  }

  // Storage to save result;
  $storage = array();

  if (isset($conditions['name of element'])) {
    $storage[] = _variation_modify_element_conditions_name_of_element($element, $conditions['name of element']);
  }
  if (isset($conditions['level of deep'])) {
    $storage[] = _variation_modify_element_conditions_level_of_deep($element, $conditions['level of deep']);
  }
  if (isset($conditions['property of element'])) {
    $storage[] = _variation_modify_element_conditions_property_of_element($element, $conditions['property of element']);
  }

  // Last.
  if (empty($storage)) {
    return false;
  }
  switch ($operator) {
    case 'or':
      return in_array(TRUE, $storage);
      break;

    case 'and':
      return !in_array(FALSE, $storage);
      break;
  }
}

function _variation_modify_element_conditions_name_of_element($element, $conditions) {
  // Harus terdapat info di #array_parents
  if (!isset($element['#array_parents']) || !is_array($element['#array_parents']) || empty($element['#array_parents'])) {
    return false;
  }
  // Shortcut method.
  if (is_string($conditions)) {
    $conditions = array(
      'operator' => 'equals',
      'value' => $conditions,
    );
  }

  // Validate operator.
  $operator = isset($conditions['operator']) ? $conditions['operator'] : 'equals';
  if (!in_array($operator, array('equals', 'negate'))) {
    return false;
  }

  // Validate value.
  if (!isset($conditions['value']) || !is_string($conditions['value']) || empty($conditions['value'])) {
    return false;
  }

  // Matching.
  $array_parents = $element['#array_parents'];
  $element_name = array_pop($array_parents);
  switch ($operator) {
    case 'equals':
      $result = $element_name === $conditions['value'];
      break;

    case 'negate':
      $result = $element_name !== $conditions['value'];
      break;
  }
  return $result;
}

function _variation_modify_element_conditions_level_of_deep($element, $conditions) {
  // Harus terdapat info di #array_parents
  if (!isset($element['#array_parents']) || !is_array($element['#array_parents']) || empty($element['#array_parents'])) {
    return false;
  }
  // Shortcut method.
  if (is_numeric($conditions)) {
    $conditions = array(
      'operator' => '=',
      'value' => $conditions,
    );
  }

  // Validate operator.
  $operator = isset($conditions['operator']) ? $conditions['operator'] : '=';
  if (!in_array($operator, array('=', '>', '<', '>=', '<='))) {
    return false;
  }
  // mydebug($element,'$element @ ' . __FUNCTION__, 'f:debug_5');

  // Validate value.
  if (!isset($conditions['value']) || !is_numeric($conditions['value'])) {
    return false;
  }

  // Matching.
  $value = (int) $conditions['value'];
  $array_parents = $element['#array_parents'];

  $current_level = count($array_parents);
  switch ($operator) {
    case '=':
      $result = $current_level == $value;
      break;

    case '>':
      $result = $current_level > $value;
      break;

    case '<':
      $result = $current_level > $value;
      break;

    case '>=':
      $result = $current_level >= $value;
      break;

    case '<=':
      $result = $current_level <= $value;
      break;
  }
  return $result;
}

function _variation_modify_element_conditions_property_of_element($element, $conditions) {
  // Validate operator.
  $operator = isset($conditions['operator']) ? $conditions['operator'] : 'or';
  if (!in_array($operator, array('or', 'and'))) {
    return false;
  }
  unset($conditions['operator']);

  // Storage to save result;
  $storage = array();

  $properties = array_keys($conditions);
  foreach ($properties as $property) {
    if (!isset($element[$property])) {
      $storage[] = FALSE;
      continue;
    }
    $original_value = $element[$property];
    $match_value = $conditions[$property];
    if (!is_array($original_value) && !is_array($match_value)) {
      $original_value = (string) $original_value;
      $match_value = (string) $match_value;
      $storage[] = $original_value == $match_value;
    }
    if (!is_array($original_value) && is_array($match_value)) {
      $storage[] = in_array($original_value, $match_value);
    }
    elseif (is_array($original_value) && !is_array($match_value)) {
      $storage[] = in_array($match_value, $original_value);
    }
    elseif (is_array($original_value) && is_array($match_value)) {
      $match = false;
      foreach ($match_value as $item) {
        if (in_array($item, $original_value)) {
          $match = true;
          break;
        }
      }
      $storage[] = $match;
    }
  }

  // Last.
  if (empty($storage)) {
    return false;
  }
  switch ($operator) {
    case 'or':
      return in_array(TRUE, $storage);
      break;

    case 'and':
      return !in_array(FALSE, $storage);
      break;
  }
}

function _variation_modify_element_actions(&$element, $actions) {
  foreach ($actions as $key => $action) {
    if (!is_numeric($key)) {
      continue;
    }
    if (isset($action['merge value'])) {
      _variation_modify_element_actions_merge_value($element, $action['merge value']);
    }
    if (isset($action['replace value'])) {
      _variation_modify_element_actions_replace_value($element, $action['replace value']);
    }
    if (isset($action['insert value'])) {
      _variation_modify_element_actions_insert_value($element, $action['insert value']);
    }
    if (isset($action['remove value'])) {
      _variation_modify_element_actions_remove_value($element, $action['remove value']);
    }
  }
}

function _variation_modify_element_actions_merge_value(&$element, $actions) {
  // Validate.
  if (!is_array($actions) || empty($actions)) {
    return;
  }
  // Do.
  foreach ($actions as $key => $values) {
    $element = array_merge_recursive($element, array($key => $values));
  }
}

function _variation_modify_element_actions_replace_value(&$element, $actions) {

  if (isset($actions['in array'])) {
    // Validate.
    if (!isset($actions['in array']['key']) || !isset($actions['in array']['old value']) || !isset($actions['in array']['new value'])) {
      return false;
    }
    $key = $actions['in array']['key'];
    $old_value = $actions['in array']['old value'];
    $new_value = $actions['in array']['new value'];
  }
  else {
    // Validate.
    if (!isset($actions['key']) || !isset($actions['old value']) || !isset($actions['new value'])) {
      return false;
    }
    $key = $actions['key'];
    $old_value = $actions['old value'];
    $new_value = $actions['new value'];
  }
  
  
  if (isset($actions['in array'])) {
    // Validate.
    if (!isset($actions['in array']['key']) || !isset($actions['in array']['old value']) || !isset($actions['in array']['new value'])) {
      return false;
    }
    $key = $actions['in array']['key'];
    $old_value = $actions['in array']['old value'];
    $new_value = $actions['in array']['new value'];
    if (!isset($element[$key]) || !is_array($element[$key])) {
      return false;
    }
    // Replace means update if exists and insert if not exists
    if (in_array($old_value, $element[$key])) {
      $i = array_search($old_value, $element[$key]);
      $element[$key][$i] = $new_value;
    }
    else{
      $element[$key][] = $new_value;
    }
  }
  else {
    // Validate.
     // if (!isset($actions['key']) || !isset($actions['old value']) || !isset($actions['new value'])) {
     if (!isset($actions['key'])) {
      return false;
    }
    
    $key = $actions['key'];
    $old_value = isset($actions['old value']) ? $actions['old value'] : '';
    $new_value = isset($actions['new value']) ? $actions['new value'] : '';
    $value = isset($actions['value']) ? $actions['value'] : '';
    
    if (!empty($value)) {
      $element[$key] = $value;
    }
    elseif (!empty($old_value) && !empty($new_value)) {
      if ($element[$key] == $old_value) {
        $element[$key] = $new_value;
      }
    }    
  }
}

function _variation_modify_element_actions_insert_value(&$element, $actions) {

  if (isset($actions['in array'])) {
    // Validate.
    if (!isset($actions['in array']['key']) || !isset($actions['in array']['value'])) {
      return false;
    }
    $key = $actions['in array']['key'];
    $value = $actions['in array']['value'];
    // Insert only if not exists.
    if (!in_array($value, $element[$key])) {      
      $element[$key][] = $value;
    }
  }
  else{
    
  }
  // mydebug($element,'$element @ ' . __FUNCTION__, 'f:debug_3');
}

function _variation_modify_element_actions_remove_value(&$element, $actions) {

  if (isset($actions['in array'])) {
    // Validate.
    if (!isset($actions['in array']['key']) || !isset($actions['in array']['value'])) {
      return false;
    }
    $key = $actions['in array']['key'];
    $value = $actions['in array']['value'];
    // Insert only if not exists.
    if (in_array($value, $element[$key])) {
      $i = array_search($value, $element[$key]);
      unset($element[$key][$i]);      
    }
  }
}
